//
// Created by piros on 30.12.2021.
//
#include "../../include/transform/rotate.h"
#include <stdlib.h>

static size_t find_new_pixel_address(size_t line, size_t column, struct image const* img){
    return line * img->width + column;
}

static size_t find_old_pixel_address(size_t line, size_t column, struct image const* img){
    return (img->height - column - 1) * img->width + line;
}

struct image rotate(struct image const* source ){
    uint32_t width = source->height;
    uint32_t height = source->width;

   struct image image = image_create_empty(width, height);

    for (size_t i = 0; i < height; ++i){
        for (size_t j = 0; j < width; ++j){
            image.data[find_new_pixel_address(i, j, &image)] = source->data[find_old_pixel_address(i, j, source)];
        }
    }

    return image;
}
