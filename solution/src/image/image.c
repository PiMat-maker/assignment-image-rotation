//
// Created by piros on 30.12.2021.
//
#include "../../include/image/image.h"
#include <malloc.h>


struct image image_create_empty(uint32_t width, uint32_t height){
    struct image image;

    image.width = width;
    image.height = height;
    image.data = (struct pixel*) malloc(sizeof (struct pixel) * width * height);

    return image;
}
