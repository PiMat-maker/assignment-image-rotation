//
// Created by piros on 31.12.2021.
//
#include "../../include/formats/bmp.h"
#include "../../include/image/image.h"
#include "../../include/util/utils.h"
#include <bits/types/FILE.h>
#include <stdio.h>
#include <unistd.h>


struct image read_image(FILE** in, const char* path, enum read_status (*from_format)(FILE* in, struct image* img)){

    struct image image;
    char dir[1000];
    getcwd(dir, 1000);
    *in = fopen(path, "rb");
    if (from_format(*in, &image) != READ_OK){
        image.width = 0;
        image.height = 0;
    }
    return image;

}

enum write_status write_image(FILE** out, struct image* image, const char* path, enum write_status (*to_format)(FILE* out, struct image const* img)){

    *out = fopen(path, "wb");
    return to_format(*out, image);
}
