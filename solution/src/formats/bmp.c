//
// Created by piros on 30.12.2021.
//
#include "../../include/formats/bmp.h"
#include <stdio.h>

#define BOFFBITS 54

struct __attribute__((packed)) BMPHeader{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static size_t read_bmp_header(FILE* in, struct BMPHeader* bmpHeader){
    return fread(bmpHeader, sizeof(struct BMPHeader), 1, in);
}

static size_t read_pixel_line(FILE* in, struct pixel* line, uint32_t length){
    return fread(line, sizeof(struct pixel), length, in);
}

static size_t write_pixel_line(FILE* out, struct pixel* line, uint32_t length){
    return fwrite(line, sizeof(struct pixel), length, out);
}

static uint32_t count_offset(uint32_t width){
    return (4 - ((width * 3) % 4)) % 4;
}

static enum write_status create_bmp_header(struct image const *img, struct BMPHeader *bmpHeader) {
    bmpHeader->bfType = 19778;
    bmpHeader->bfileSize = img->width * img->height * 3 + BOFFBITS;
    bmpHeader->bfReserved = 0;
    bmpHeader->bOffBits = BOFFBITS;
    bmpHeader->biSize = 40;
    bmpHeader->biWidth = img->width;
    bmpHeader->biHeight = img->height;
    bmpHeader->biPlanes = 1;
    bmpHeader->biBitCount = 24;
    bmpHeader->biCompression = 0;
    bmpHeader->biSizeImage = img->width * img->height * 3;
    bmpHeader->biXPelsPerMeter = 0;
    bmpHeader->biYPelsPerMeter = 0;
    bmpHeader->biClrUsed = 0;
    bmpHeader->biClrImportant = 0;
    return WRITE_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ){

    struct BMPHeader bmpHeader;
    if (read_bmp_header(in, &bmpHeader) != 1){
        return READ_INVALID_HEADER;
    }

    *img = image_create_empty(bmpHeader.biWidth, bmpHeader.biHeight);
    uint32_t offset = count_offset(img->width);
    for (uint32_t i = 0; i < img->height; ++i){
        if (read_pixel_line(in, img->data + i * img->width, img->width) != img->width){
            return READ_INVALID_BITS;
        }
        if (fseek(in, offset, SEEK_CUR)){
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){

    if (!out){
        return WRITE_INVALID_FILE;
    }

    struct BMPHeader bmpHeader = {0};
    create_bmp_header(img, &bmpHeader);   

    if(fwrite(&bmpHeader, sizeof(struct BMPHeader), 1, out) != 1){
        return WRITE_ERROR;
    }

    uint32_t offset = count_offset(img->width);
    char symbol = 0x00;

    for (uint32_t i = 0; i < img->height; ++i){
        if (write_pixel_line(out, img->data + i * img->width, img->width) != img->width){
            return WRITE_ERROR;
        }
        for (uint32_t j = 0; j < offset; ++j){
            if(fwrite(&symbol, sizeof(char), 1, out) != 1){
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
