#include "../include/formats/bmp.h"
#include "../include/transform/rotate.h"
#include "../include/util/utils.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {

    if (argc != 3){
        printf("Wrong input!\n");
        exit(1);
    }

    FILE* in;
    FILE* out;
    struct image image = read_image(&in, argv[1], from_bmp);

    if (fclose(in)){
        printf("Can't close the input file\n");
        free(image.data);
        return 0;
    }

    if (image.width == 0 || image.height == 0){
        printf("Empty image\n");
        free(image.data);
        return 0;
    }

    struct image result = rotate(&image);

    if (write_image(&out, &result, argv[2], to_bmp)){
        printf("Wrong write format\n");
        free(image.data);
        free(result.data);
        return 0;
    }

    if (fclose(out)){
        printf("Can't close the output file\n");
        free(image.data);
        free(result.data);
        return 0;
    }

    free(image.data);
    free(result.data);

    return 0;
}
