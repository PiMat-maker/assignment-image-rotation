//
// Created by piros on 30.12.2021.
//

#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdint.h>

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image image_create_empty(uint32_t width, uint32_t height);

#endif //LAB3_IMAGE_H
