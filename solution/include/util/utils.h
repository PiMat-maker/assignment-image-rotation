//
// Created by piros on 31.12.2021.
//

#ifndef LAB3_UTILS_H
#define LAB3_UTILS_H

#include "../formats/bmp.h"

struct image read_image(FILE** in, const char* path, enum read_status (*from_format)(FILE* in, struct image* img));

enum write_status write_image(FILE** out, struct image* image, const char* path, enum write_status (*to_format)(FILE* out, struct image const* img));

#endif //LAB3_UTILS_H
