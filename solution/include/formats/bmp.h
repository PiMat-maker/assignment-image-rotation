//
// Created by piros on 30.12.2021.
//

#ifndef LAB3_BMP_H
#define LAB3_BMP_H

#include "../image/image.h"
#include  <stdint.h>
#include <stdio.h>


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 3,
    READ_INVALID_BITS = 1,
    READ_INVALID_HEADER = 2
};



enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1,
    WRITE_INVALID_FILE = 2
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //LAB3_BMP_H
