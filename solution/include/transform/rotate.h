//
// Created by piros on 30.12.2021.
//

#ifndef LAB3_ROTATE_H
#define LAB3_ROTATE_H

#include "../image/image.h"

struct image rotate(struct image const* source );

#endif //LAB3_ROTATE_H
